#!/usr/bin/python3

import json
from time import time
from flask import Flask, redirect, request


app = Flask(__name__)


class Message(dict):
    def __init__(self, name, message):
        self.name = name
        self.message = message
        self.time = time()


messages = []


def add_message(name, message):
    m = Message(name, message)
    messages.append(m)
    print(json.dumps(m.__dict__))
    if len(messages) > 30:
        del messages[0]


@app.route('/')
def root():
    return redirect("/static/index.html")


@app.route('/getmessages', methods=['GET'])
def getmessages():
    t = request.args.get("time")
    if float(t) == 0 and len(messages) > 0:
        return json.dumps(messages[-1].__dict__)
    for m in messages:
        if m.time > float(t):
            return json.dumps(m.__dict__)
    return ""


@app.route('/sendmessage', methods=['POST', 'GET'])
def sendmessage():
    add_message(request.form["name"], request.form["message"])
    return ""


if __name__ == '__main__':
    app.run(port="12345")
